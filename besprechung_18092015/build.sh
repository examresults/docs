#!/bin/bash

cmake . &> /dev/null
if [ $? != 0 ];
then
    echo "cmake failure"
    exit
else
    echo "cmake okay"
fi

make &> /dev/null
if [ $? != 0 ];
then
    echo "make failure"
    exit
else
    echo "make okay"
fi
