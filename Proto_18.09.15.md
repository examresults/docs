# Protokoll vom 18.09.2015

## Dokumentation
 + JT -> Vortrag 15min

## Team
Zwei paralelle Sprints/Teams
 + Sprint: Entwicklungssprint; Infra -> PO, Master
 + Sprint:  Konzept; Test; Doku -> PO, Master
 + Leitung: PO, PO
 + Master != PO

## Stakeholder
 + RZ klären
  + Systemübersicht
  + Termin Müller
    + zweiteilig
    + 2 Leute
  + API - Parser
  + 

## Doku 
 + UML/ Paketdiagramm
  + Modulsicht
  + Aktivitätsdiagramm
  + Klassendiagramm
 
