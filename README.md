# Vorbereitung auf Projekt #

 + einzelne Präsentationen

# Bauen der LaTeX-Files #

## Linux ##

```
#!bash

cmake .
make
```


pdf befindet sich in Unterordner build/

## Benötigte Pakete: ##

* texlive(-full)
* cmake
* make